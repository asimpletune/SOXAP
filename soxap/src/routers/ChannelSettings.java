package routers;

import java.net.InetAddress;

import paxos.Main;


public class ChannelSettings 
{	
	public static ChannelSettings defaultChannel = new ChannelSettings(Channels.SERVER);
	
	public int port;
	public InetAddress address;	
	public ChannelSettings(Channels channel)
	{
		switch (channel)
		{
			case CLIENT:
				this.port = Main.CLIENT_PORT;
				this.address = Main.MCAST_CLIENT; 			
				break;
			case SERVER:
				this.port = Main.SERVER_PORT;
				this.address = Main.MCAST_SERVER;			
				break;
			default:
				throw new IllegalArgumentException("Something weird happened");			
		}
	}
	
	public enum Channels
	{
		CLIENT, SERVER
	}
}
