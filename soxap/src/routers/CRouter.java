package routers;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.LinkedList;
import java.util.Queue;

import paxos.HeartbeatThread;
import paxos.PServer;
import paxos.ServerStates;

import commands.ClientIn;
import commands.ClientOut;
import commands.Cmd;
import commands.Command;
import commands.Prepare;
import commands.Timeout;

//TODO
public class CRouter extends Channel
{
	private PServer parent;
	private Queue<ClientOut> msgBuffer;
	public CRouter(PServer parent, ChannelSettings channel) throws IOException 
	{
		super(channel);
		this.parent = parent;		
		this.socket.setSoTimeout(HeartbeatThread.DEFAULT_HEARTBEAT_INTERVAL << 2);
		msgBuffer = new LinkedList<ClientOut>();
	}

	@Override
	public void switchBoard(Command cmd) throws IOException 
	{		
		switch (cmd.command())
		{
		
		case Cmd.CLIENT_OUT:
		{
			msgBuffer.offer((ClientOut) cmd);
			if (parent.getState() == ServerStates.COLLECTING_CLIENT_OUT)
			{
				parent.sRouter.stateSwitchBoard(cmd);
			}
			break;
		}
		
		case Cmd.TIMEOUT:
		{
			System.out.println(String.format("CROUTER, routing %s", cmd));
			broadcast(new Prepare(parent.prepareNumber + 1));
			break;
		}
		
		}		
	}
	
	@Override
	public Command getIncomingCmd() throws IOException
	{
		try
		{
			return super.getIncomingCmd();
		}
		catch (SocketTimeoutException s)
		{
			return new Timeout();
		}			
	}
	
	@Override
	public void broadcast(Command cmd) throws IOException
	{
		parent.println("broadcasting " + cmd);
		super.broadcast(cmd);
		parent.println("sent " + cmd);
	}

}
