package routers;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

import paxos.Main;

import commands.Command;


public abstract class Channel extends Thread
{		

	BufferedReader reader;
	MulticastSocket socket;
	public int port;
	public InetAddress address;
	byte[] readBuffer;
	Object readLock;
	Object writeLock;
	public boolean verbose;

	public Channel(ChannelSettings channel) throws IOException 
	{	
		super();
		synchronized(this)
		{			
			port = channel.port;
			address = channel.address;			
			socket = new MulticastSocket(port);
			socket.joinGroup(address);
			readLock = new Object();
			writeLock = new Object();
		}
	}

	@Override
	public void run()
	{			
		try
		{																					
			while (true) 
			{							
				Command cmd = getIncomingCmd();				
				switchBoard(cmd);				
			} 
		} 
		catch(IOException e)
		{
			System.out.println("Error in multicast listen: " + e);
		}
		finally
		{
			socket.close();
		}
	}

	public Command getIncomingCmd() throws IOException
	{
		synchronized(readLock)
		{			
			readBuffer = new byte[Main.READ_BUFFER_SIZE];
			DatagramPacket dgram = new DatagramPacket(readBuffer, readBuffer.length);
			socket.receive(dgram);

			ByteArrayInputStream bis = new ByteArrayInputStream(readBuffer);
			ObjectInput in = null;
			Command command = null;
			try {
				in = new ObjectInputStream(bis);
				command = (Command)in.readObject(); 
			} catch (ClassNotFoundException e) {
//				System.out.println("Error in getIncomingCmd: " + e.getMessage());
			} finally {
				bis.close();
				in.close();
			}
			return command;
		}
	}

	public abstract void switchBoard(Command cmd) throws IOException;


	public void broadcast(Command cmd) throws IOException
	{			
		synchronized(writeLock)
		{			

			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutput out = null;
			byte[] commandBytes = null;
			try {
				out = new ObjectOutputStream(bos);   
				out.writeObject(cmd);
				commandBytes = bos.toByteArray();
			} finally {
				out.close();
				bos.close();
			}

			DatagramPacket outPacket = new DatagramPacket(commandBytes, commandBytes.length, address, port);			
			socket.send(outPacket);
		}
	}	
	
	public void println(String s)
	{
		if (verbose)			
			System.out.println(s);
	}
}
