package paxos;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;

import commands.ClientOut;
import commands.Cmd;
import commands.Command;

import routers.Channel;
import routers.ChannelSettings;

public class PClient
{                   
    public PClient(ChannelSettings channel) throws IOException
    {
        ListenerThread t1 = new ListenerThread(channel);
        KeyboardListener t2 = new KeyboardListener(channel);
        t1.start();
        t2.start();
    }

    public class ListenerThread extends Channel
    {       
        public ListenerThread(ChannelSettings channel) throws IOException
        {           
            super(channel);              
        }

        @Override
        public void switchBoard(Command cmd) throws IOException 
        {                                                  
            switch(cmd.command())
            {
                case Cmd.CLIENT_IN:
                {                        
                    System.out.println(cmd);
                    break;
                }
                default:
                {                       
                	break;
                }
            }            
        }
    }

    public class KeyboardListener extends Thread
    {       
        DatagramSocket socket;
        DatagramPacket dgram;
        public int port;
        public InetAddress address;
        byte[] readBuffer;
        BufferedReader reader;

        public KeyboardListener(ChannelSettings channel) throws IOException
        {
//            super(channel);
            reader = new BufferedReader(new InputStreamReader(System.in));
            socket = new DatagramSocket();
            port = channel.port;
            address = channel.address;        
        }       

        @Override
        public void run()
        {
            while(true) {
                try
                { 
                    Command cmd = new ClientOut(reader.readLine());
                	ByteArrayOutputStream bos = new ByteArrayOutputStream();
        			ObjectOutput out = null;
        			byte[] commandBytes = null;
        			try {
        				out = new ObjectOutputStream(bos);   
        				out.writeObject(cmd);
        				commandBytes = bos.toByteArray();
        			} finally {
        				out.close();
        				bos.close();
        			}

        			dgram = new DatagramPacket(commandBytes, commandBytes.length, address, port);	
                    System.out.println("CLIENT SENDING: " + cmd);
        			socket.send(dgram);
                	
                	
                    //readBuffer = (Cmd.CLIENT_OUT + " " + reader.readLine()).getBytes();
                    //dgram = new DatagramPacket(readBuffer, readBuffer.length, address, port);
                    //socket.send(clientOut);           
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }                                               
            }
        }
    }
}