package paxos;

import java.io.IOException;

import commands.Command;
import commands.Heartbeat;

import routers.Channel;
import routers.ChannelSettings;

//TODO: test
public class HeartbeatThread extends Channel
{
	public static final int DEFAULT_HEARTBEAT_INTERVAL = 1000; 	
	public boolean halt;
	public HeartbeatThread(ChannelSettings channel) throws IOException
	{
		super(channel);
		halt = false;
	}
	
	@Override
	public void run()
	{				
		while(!halt)
		{
			try 
			{
				sleep(DEFAULT_HEARTBEAT_INTERVAL);
				broadcast(new Heartbeat());
			} 
			catch (InterruptedException | IOException e) 
			{			
				System.out.println("EXCEPTION HEARTBEAT_THREAD");
				e.printStackTrace();
			}
		}
	}

	@Override
	public void switchBoard(Command cmd) throws IOException {/* doesn't need to do anything */}

}
