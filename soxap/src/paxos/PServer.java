package paxos;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.net.DatagramPacket;
import java.util.HashSet;
//import java.util.Hashtable;

import routers.CRouter;
import routers.ChannelSettings;
import routers.SRouter;

import commands.Command;
import commands.History;


public class PServer 
{		
	public int serverID;
	public boolean verbose;
	public int prepareNumber;
	public History history;	
	private ServerStates state;
	public HashSet<History> collectedHistories;
	public int ackCount;
	public CleanupAndReplayThread thread;	
	public SRouter sRouter;
	public CRouter cRouter;
	public HeartbeatThread heartbeat;
	
	public PServer(int serverID) throws IOException
	{		
		// state params
		this.serverID = serverID;		
		this.verbose = Main.verbosePServer;		
		history = new History(this);
		collectedHistories = new HashSet<History>();
		ackCount = 0;
		sRouter = new SRouter(this, ChannelSettings.defaultChannel);
		cRouter = new CRouter(this, ChannelSettings.defaultChannel);
		
		// starting the server
		sRouter.start();
		cRouter.start();		
		heartbeat = new HeartbeatThread(ChannelSettings.defaultChannel);
		setState(ServerStates.INITIAL);
		//loadFromDisk();
		println("server created with ID# " + serverID);
	}
	
	public void saveStateToDisk() throws IOException
	{
		StateForDisk sfd = new StateForDisk(verbose, prepareNumber, history);
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = null;
		byte[] stateBytes = null;
		try {
			out = new ObjectOutputStream(bos);   
			out.writeObject(sfd);
			stateBytes = bos.toByteArray();
		} finally {
			out.close();
			bos.close();
		}

		OutputStream fileOut = new FileOutputStream(serverID + "");
		fileOut.write(stateBytes);
		fileOut.flush();
		fileOut.close();
	}
	
	public boolean loadFromDisk()
	{
		try 
		{
			RandomAccessFile f = new RandomAccessFile(serverID + "", "r");
			byte[] data = new byte[(int)f.length()];
			f.read(data);
			
			ByteArrayInputStream bis = new ByteArrayInputStream(data);
			ObjectInput in = null;
			StateForDisk state = null;
			try {
				in = new ObjectInputStream(bis);
				state = (StateForDisk)in.readObject(); 
				
				this.verbose = state.verbose;
				this.prepareNumber = state.prepareNumber;
				this.history = state.history;
			} catch (ClassNotFoundException e) {
				return false;
			} finally {
				bis.close();
				in.close();
			}
			
			return true;
		} 
		catch (Exception e)
		{
			return false;
		}

	}

	public static int Prepare2Leader(int prepareNumber)
	{
		return prepareNumber % Main.NUM_SERVERS;
	}
	
	public ServerStates getState()
	{
		return state;
	}
	
	public void setState(ServerStates newState)
	{
		if (verbose)
			System.out.println("Setting state to: " + newState);
		state = newState;
	}
	
	public boolean isLeader()
	{
		return serverID == prepareNumber % Main.NUM_SERVERS;
	}
	
	public void add(Command cmd)
	{
		switch (state)
		{
		
		case INITIAL:
		{
			throw new IllegalStateException("Can't add while in the \"initial\" state");			
		}
		case COLLECTING_HISTORY:
		{
			println(String.format("Adding %s to collectedHistory", cmd));			
			collectedHistories.add((History)cmd);
			println(String.format("collectedHistory is of size %d", collectedHistories.size()));
			break;
		}
		case COLLECTING_ACKS:
		{
			ackCount++;
			break;
		}
		case COLLECTING_CLIENT_OUT:
		{
			// ignore adding when not in that state
//			throw new IllegalStateException("Can't add while in the \"collecting_client_out\" state");			
		}
		default:
		{			
			break;
		}
		
		}
	}
	
	public void resetAcks()
	{
		ackCount = 0;
		println("Reset ack count");
	}
	
	public void resetCollectedHistories()
	{
		collectedHistories = new HashSet<History>();
		println("Reset collected histories");
	}
	
	public boolean quorum()
	{
		println("quorum is " + Main.QUORUM);
		switch (state)
		{
		
		case INITIAL:
		{
			throw new IllegalStateException("Can't have quorum while in the \"initial\" state");			
		}
		case COLLECTING_HISTORY:
		{
			return collectedHistories.size() == Main.QUORUM;			
		}
		case COLLECTING_ACKS:
		{
			return ackCount == Main.QUORUM;			
		}
		case COLLECTING_CLIENT_OUT:
		{
			return false;
//			throw new IllegalStateException("Can't have quorum while in the \"collecting_client_out\" state");			
		}
		default:
		{			
			println("Trying to collect quorum on default state");
			return false;
//			throw new IllegalStateException("Trying to find quorum while in a state that switched on default");
		}
		
		}
	}
	
	
	public void println(String s) 
	{
		if (verbose)
			System.out.println(s);		
	}
}
