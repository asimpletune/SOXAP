package paxos;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import routers.ChannelSettings;


public class Main
{
	public static InetAddress MCAST_CLIENT;
	public static InetAddress MCAST_SERVER;
	public static final int CLIENT_PORT = 6789;
	public static final int SERVER_PORT = 6790;
	public static final int READ_BUFFER_SIZE = 2056;
	public static int NUM_SERVERS;	
	public static int QUORUM;
	
	public static boolean verbosePServer;
	

	public static void main(String[] args) throws InterruptedException, ClassNotFoundException, NumberFormatException, IOException 
	{
		MCAST_CLIENT = InetAddress.getByName("224.0.0.1");
		MCAST_SERVER = InetAddress.getByName("224.0.0.2");				
	
		switch(args[0].toLowerCase()) 
		{		
			case "server":
			{
				if (args.length >= 3)
				{
					NUM_SERVERS = Integer.parseInt(args[2]);
					QUORUM = (NUM_SERVERS / 2) + 1;
				}
				System.out.println("server");
				if (args.length > 3)
					verbosePServer = args[3].equals("-v");
				startServer(Integer.parseInt(args[1]));
				break;
			}
			case "client":
			{	
				new PClient(ChannelSettings.defaultChannel);
				break;
			}
			default:
			{		
				System.out.println("MAIN DEFAULT");
			}
		}				
	}
	
	public static void startServer(int serverID) throws IOException, InterruptedException
	{
		new PServer(serverID);
	}
}
