package paxos;

import java.io.Serializable;

import commands.History;

public class StateForDisk implements Serializable {

	private static final long serialVersionUID = 4017876521306557148L;
	public boolean verbose;
	public int prepareNumber;
	public History history;	
	
	public StateForDisk(boolean verbose, int prepareNumber, History history)
	{
		this.verbose = verbose;
		this.prepareNumber = prepareNumber;
		this.history = history;
	}
	
}
