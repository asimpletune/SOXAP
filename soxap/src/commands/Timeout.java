package commands;

import java.io.Serializable;

public class Timeout implements Serializable, Command 
{
	private static final long serialVersionUID = 837876765096180466L;

	@Override
	public String command() 
	{
		return Cmd.TIMEOUT;
	}
	
	@Override
	public String toString()
	{
		return String.format("%s", Cmd.TIMEOUT);
	}
}