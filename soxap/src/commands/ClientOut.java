package commands;
import java.io.Serializable;



public class ClientOut implements Serializable, Command 
{
	private static final long serialVersionUID = 1053385989160643796L;
	protected String message;
	
	public ClientOut(String message)
	{
		this.message = message;
	}
	
	public String message()
	{
		return message;
	}
	
	@Override
	public String command() 
	{
		return Cmd.CLIENT_OUT;
	}
	
	@Override
	public String toString()
	{
		return String.format("%s %s", Cmd.CLIENT_OUT, message);
	}
}
