package commands;
import java.io.Serializable;


public class ClientIn implements Serializable, Command {

	private static final long serialVersionUID = 7725244120029489408L;
	protected String message;
	
	public ClientIn(String message){
		this.message = message;
	}
	
	public String message(){
		return message;
	}
	
	@Override
	public String command() {
		return Cmd.CLIENT_IN;
	}
	
	@Override
	public String toString()
	{
		return String.format("%s %s", Cmd.CLIENT_IN, message);
	}
}
