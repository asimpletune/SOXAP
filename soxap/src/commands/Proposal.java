package commands;

import java.io.Serializable;


public class Proposal implements Serializable, Command
{
	
	private static final long serialVersionUID = 3951779806820029711L;
	private int prepareNum;
	private String value;

	public Proposal(int prepareNumber, String msg)
	{
		assert prepareNumber > -1;
		prepareNum = prepareNumber;
		value = msg;
	}

	public int prepareNumber()
	{
		return prepareNum;
	}

	public String value()
	{
		return value;

	}

	public static Proposal parse(String proposal)
	{
		try
		{
			int prepNum = Integer.parseInt(proposal.substring(1, proposal.indexOf(',')));
			String value = proposal.substring(proposal.indexOf(',') + 1, proposal.length() - 1);
			return new Proposal(prepNum, value);
		}
		catch (Exception e)
		{
			return null;
		}		
	}
	
	public static Proposal noop(int prepareNumber)
	{
		return new Proposal(prepareNumber, Cmd.NOOP);
	}

	@Override
	public String toString()
	{
		return String.format("(%d,%s)", prepareNum, value);
	}

	@Override
	public String command() 
	{
		return Cmd.PREPARE;
	}
}