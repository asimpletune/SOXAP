package commands;

import java.io.Serializable;

public class Accept implements Serializable, Command 
{
	private static final long serialVersionUID = 1135764981563949017L;
	protected int leaderId;
	protected int slotNumber;
	protected Proposal proposal;
	protected boolean share;
	
	public Accept(int leaderId, int slotNumber, Proposal proposal, boolean share)
	{
		this.leaderId = leaderId;
		this.slotNumber = slotNumber;
		this.proposal = proposal;
		this.share = share;
	}
	
	public int leaderId() 
	{
		return leaderId;
	}

	public void setLeaderId(int leaderId) 
	{
		this.leaderId = leaderId;
	}

	public int slotNumber() 
	{
		return slotNumber;
	}

	public void setSlotNumber(int slotNumber) 
	{
		this.slotNumber = slotNumber;
	}

	public Proposal proposal() 
	{
		return proposal;
	}

	public void setProposal(Proposal proposal) 
	{
		this.proposal = proposal;
	}

	public boolean share() 
	{
		return share;
	}

	public void setShare(boolean share) 
	{
		this.share = share;
	}

	@Override
	public String command() 
	{
		return Cmd.ACCEPT;
	}
	
	@Override
	public String toString()
	{
		return String.format("%s %d %d %s %b", Cmd.ACCEPT, leaderId, slotNumber, proposal, share);
	}


}
