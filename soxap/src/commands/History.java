package commands;
import java.io.Serializable;
import java.util.ArrayList;

import paxos.PServer;



public class History extends ArrayList<Proposal> implements Serializable, Command
{
	private static final long serialVersionUID = -8631533837209739744L;
	int serverID;
	
	public History(PServer parent)
	{
		super();
		serverID = parent.serverID;
	}	

	@Override
	public void add(int slot, Proposal p)
	{
		System.out.println(String.format("Adding %s in slot %d", p.value(), slot));		
		super.add(slot, p);
	}
	
	@Override
	public String toString()
	{
		String result = "";
		for(Proposal p : this)
		{
			result += p.toString() + "|";
		}
		return result.equals("") ? "NOOP" : result;	
	}

	@Override
	public String command() 
	{
		return Cmd.HISTORY;
	}
	
	@Override
	public int hashCode()
	{
		return serverID;
	}
}