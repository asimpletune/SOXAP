package commands;

public class Cmd 
{
	public static final String TIMEOUT = "TIMEOUT";
	public static final String PREPARE = "PREPARE";
	public static final String HISTORY = "HISTORY";
	public static final String NOOP = "NOOP";
	public static final String CLIENT_IN = ">CLIENT";
	public static final String ACCEPT = "ACCEPT";
	public static final String ACK = "ACK";
	public static final String HEARTBEAT = "HEARTBEAT";
	public static final String CLIENT_OUT = "CLIENT>";
}
