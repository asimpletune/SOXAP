package commands;

import java.io.Serializable;

public class Heartbeat implements Serializable, Command 
{
	private static final long serialVersionUID = 5920597775966640256L;

	@Override
	public String command() 
	{
		return Cmd.HEARTBEAT;
	}
	
	@Override
	public String toString()
	{
		return String.format("%s", Cmd.HEARTBEAT);
	}

}
