package commands;

import java.io.Serializable;

public class Prepare implements Serializable, Command  
{
	private static final long serialVersionUID = -2233958633628033356L;
	private int prepareNumber;
	
	public Prepare(int prepareNumber)
	{
		this.prepareNumber = prepareNumber;
	}
	
	public int prepareNumber()
	{
		return prepareNumber;
	}

	
	@Override
	public String command() 
	{
		return Cmd.PREPARE;
	}	
	
	@Override
	public String toString()
	{
		return String.format("%s %d", Cmd.PREPARE, prepareNumber);
	}
}
