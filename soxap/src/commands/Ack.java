package commands;

import java.io.Serializable;


public class Ack implements Serializable, Command
{
	private static final long serialVersionUID = -4185730875566986837L;
	protected Accept accept;
	protected int senderId;
	
	public Ack(Accept accept, int senderId)
	{
		this.accept = accept;
		this.senderId = senderId;
	}
	
	public Accept accept() 
	{
		return accept;
	}

	public void setAccept(Accept accept) 
	{
		this.accept = accept;
	}

	public int senderId() {
		return senderId;
	}

	public void setSenderId(int senderId) 
	{
		this.senderId = senderId;
	}

	@Override
	public String command() 
	{
		return Cmd.ACK;
	}
	
	@Override
	public String toString()
	{
		return String.format("%s %d %s", Cmd.ACK, senderId, accept);
	}
	
}
