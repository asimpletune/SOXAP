package paxos;
import java.io.IOException;
import java.util.HashSet;
//import java.util.Hashtable;

import routers.CRouter;
import routers.ChannelSettings;
import routers.SRouter;

import commands.Command;
import commands.History;


public class PServer 
{		
	public int serverID;
	public boolean verbose;
	public int prepareNumber;
	public History history;	
	private ServerStates state;
	public HashSet<History> collectedHistories;
	public int ackCount;
	public CleanupAndReplayThread thread;	
	public SRouter sRouter;
	public CRouter cRouter;
	
	public PServer(int serverID) throws IOException
	{
		// state params
		this.serverID = serverID;		
		this.verbose = Main.verbosePServer;		
		history = new History();
		collectedHistories = new HashSet<History>();
		ackCount = 0;
		sRouter = new SRouter(this, ChannelSettings.defaultChannel);
		cRouter = new CRouter(this, ChannelSettings.defaultChannel);
		
		// starting the server
		sRouter.start();
		cRouter.start();		
		setState(ServerStates.INITIAL);
		println("server created with ID# " + serverID);
	}

	public static int Prepare2Leader(int prepareNumber)
	{
		return prepareNumber % Main.NUM_SERVERS;
	}
	
	public ServerStates getState()
	{
		return state;
	}
	
	public void setState(ServerStates newState)
	{
		if (verbose)
			System.out.println("Setting state to: " + newState);
		state = newState;
	}
	
	public boolean isLeader()
	{
		return serverID == prepareNumber % Main.NUM_SERVERS;
	}
	
	public void add(Command cmd)
	{
		switch (state)
		{
		
		case INITIAL:
		{
			throw new IllegalStateException("Can't add while in the \"initial\" state");			
		}
		case COLLECTING_HISTORY:
		{
			collectedHistories.add((History)cmd);
			break;
		}
		case COLLECTING_ACKS:
		{
			ackCount++;
			break;
		}
		case COLLECTING_CLIENT_OUT:
		{
			throw new IllegalStateException("Can't add while in the \"collecting_client_out\" state");			
		}
		default:
		{			
			break;
		}
		
		}
	}
	
	public void resetAcks()
	{
		ackCount = 0;
	}
	
	public boolean quorum()
	{
		switch (state)
		{
		
		case INITIAL:
		{
			throw new IllegalStateException("Can't have quorum while in the \"initial\" state");			
		}
		case COLLECTING_HISTORY:
		{
			return collectedHistories.size() == Main.QUORUM;			
		}
		case COLLECTING_ACKS:
		{
			return ackCount == Main.QUORUM;			
		}
		case COLLECTING_CLIENT_OUT:
		{
			throw new IllegalStateException("Can't have quorum while in the \"collecting_client_out\" state");			
		}
		default:
		{			
			throw new IllegalStateException("Trying to find quorum while in a state that switched on default");
		}
		
		}
	}
	
	
	private void println(String s) 
	{
		if (verbose)
			System.out.println(s);		
	}
}
