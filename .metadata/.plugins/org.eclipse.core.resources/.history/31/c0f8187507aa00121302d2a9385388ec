package paxos;
import java.io.IOException;
import java.util.HashSet;
//import java.util.Hashtable;

import routers.CRouter;
import routers.ChannelSettings;
import routers.SRouter;

import commands.Command;
import commands.History;


public class PServer 
{		
	public int serverID;
	public boolean verbose;
	public int prepareNumber;
	public History history;	
	private ServerStates state;
	public HashSet<History> collectedHistories;
	public int ackCount;
	public CleanupAndReplayThread thread;	
	public SRouter sRouter;
	public CRouter cRouter;
	public HeartbeatThread heartbeat;
	
	public PServer(int serverID) throws IOException
	{
		//check if state saved to disk
		
		// state params
		this.serverID = serverID;		
		this.verbose = Main.verbosePServer;		
		history = new History(this);
		collectedHistories = new HashSet<History>();
		ackCount = 0;
		sRouter = new SRouter(this, ChannelSettings.defaultChannel);
		cRouter = new CRouter(this, ChannelSettings.defaultChannel);
		
		// starting the server
		sRouter.start();
		cRouter.start();		
		heartbeat = new HeartbeatThread(ChannelSettings.defaultChannel);
		setState(ServerStates.INITIAL);
		println("server created with ID# " + serverID);
	}

	public static int Prepare2Leader(int prepareNumber)
	{
		return prepareNumber % Main.NUM_SERVERS;
	}
	
	public ServerStates getState()
	{
		return state;
	}
	
	public void setState(ServerStates newState)
	{
		if (verbose)
			System.out.println("Setting state to: " + newState);
		state = newState;
	}
	
	public boolean isLeader()
	{
		return serverID == prepareNumber % Main.NUM_SERVERS;
	}
	
	public void add(Command cmd)
	{
		switch (state)
		{
		
		case INITIAL:
		{
			throw new IllegalStateException("Can't add while in the \"initial\" state");			
		}
		case COLLECTING_HISTORY:
		{
			println(String.format("Adding %s to collectedHistory", cmd));			
			collectedHistories.add((History)cmd);
			println(String.format("collectedHistory is of size %d", collectedHistories.size()));
			break;
		}
		case COLLECTING_ACKS:
		{
			ackCount++;
			break;
		}
		case COLLECTING_CLIENT_OUT:
		{
			// ignore adding when not in that state
//			throw new IllegalStateException("Can't add while in the \"collecting_client_out\" state");			
		}
		default:
		{			
			break;
		}
		
		}
	}
	
	public void resetAcks()
	{
		ackCount = 0;
		println("Reset ack count");
	}
	
	public void resetCollectedHistories()
	{
		collectedHistories = new HashSet<History>();
		println("Reset collected histories");
	}
	
	public boolean quorum()
	{
		println("quorum is " + Main.QUORUM);
		switch (state)
		{
		
		case INITIAL:
		{
			throw new IllegalStateException("Can't have quorum while in the \"initial\" state");			
		}
		case COLLECTING_HISTORY:
		{
			return collectedHistories.size() == Main.QUORUM;			
		}
		case COLLECTING_ACKS:
		{
			return ackCount == Main.QUORUM;			
		}
		case COLLECTING_CLIENT_OUT:
		{
			return false;
//			throw new IllegalStateException("Can't have quorum while in the \"collecting_client_out\" state");			
		}
		default:
		{			
			println("Trying to collect quorum on default state");
			return false;
//			throw new IllegalStateException("Trying to find quorum while in a state that switched on default");
		}
		
		}
	}
	
	
	public void println(String s) 
	{
		if (verbose)
			System.out.println(s);		
	}
}
