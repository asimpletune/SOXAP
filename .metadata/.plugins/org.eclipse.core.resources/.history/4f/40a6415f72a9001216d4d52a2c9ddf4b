import java.io.BufferedReader;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;


public abstract class Listener extends Thread{		

	BufferedReader reader;
	MulticastSocket socket;
	public int port;
	public InetAddress address;
	byte[] readBuffer;
	Object readLock;
	Object writeLock;

	public Listener(ChannelSettings channel) throws IOException 
	{	
		super();
		synchronized(this)
		{			
			port = channel.port;
			address = channel.address;			
			socket = new MulticastSocket(port);
			socket.joinGroup(address);
			readLock = new Object();
			writeLock = new Object();
		}
	}

	@Override
	public void run()
	{			
		try
		{																					
			while (true) 
			{							
				String msg = getIncomingMsg();				
				switchBoard(msg);				
			} 
		} 
		catch(IOException e)
		{
			System.out.println("Error in multicast listen: " + e);
		}
		finally
		{
			socket.close();
		}
	}

	public String getIncomingMsg() throws IOException
	{
		synchronized(readLock)
		{			
			readBuffer = new byte[Main.READ_BUFFER_SIZE];
			DatagramPacket dgram = new DatagramPacket(readBuffer, readBuffer.length);
			socket.receive(dgram);			
			return new String(dgram.getData()).trim();
		}
	}

	public abstract void switchBoard(String msg) throws IOException;


	public void broadcast(String msg) throws IOException
	{			
		synchronized(writeLock)
		{			
			System.out.println("BROADCASTING: " + msg);
			byte[] bMsg = msg.getBytes();				
			DatagramPacket out = new DatagramPacket(bMsg, bMsg.length, address, port);			
			socket.send(out);
		}
	}			
}
