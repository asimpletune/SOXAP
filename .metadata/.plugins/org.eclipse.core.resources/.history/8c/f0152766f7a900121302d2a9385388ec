package paxos;

import java.io.IOException;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Queue;

import commands.Accept;
import commands.History;
import commands.Proposal;

public class CleanupAndReplayThread extends Thread
{
	public static boolean alreadyRunning = false;
	private PServer parent;
	private Queue<Accept> acceptMsgs;
	private boolean halt;
	private CleanupAndReplayThread(PServer parent)
	{
		super();
		this.parent = parent;	
		acceptMsgs = new LinkedList<Accept>();
		halt = false;
	}
	
	public static CleanupAndReplayThread CleanAndReplay(PServer parent)
	{
		if (alreadyRunning) return null;
		alreadyRunning = true;
		return new CleanupAndReplayThread(parent);
	}
	
	@Override 
	public void run()
	{
		generateAcceptMsgs();
		try 
		{
			replayHistory();
		} 
		catch (IOException | InterruptedException e) 
		{
			System.out.println("EXCEPTION CLEANUP");
			e.printStackTrace();
		}		
		alreadyRunning = false;
		parent.println("Cleanup and replay thread exitng");
		return;
	}
	
	private void generateAcceptMsgs()
	{
		HashSet<History> collectedHistories = parent.collectedHistories;
		int numberOfSlots = parent.history.size();
		for (int slotNumber = 0; slotNumber < numberOfSlots; ++slotNumber)
		{
			Hashtable<Proposal, Integer> agreeMap = new Hashtable<Proposal, Integer>();
			boolean quorumAgreed = false;
			for (History h : collectedHistories)
			{
				Proposal p = h.get(slotNumber);
				Integer numberOfServersWhoAgreeSoFarOnP = agreeMap.get(p) == null ? 0 : agreeMap.get(p);
				numberOfServersWhoAgreeSoFarOnP++;
				agreeMap.put(p, numberOfServersWhoAgreeSoFarOnP);
				if (numberOfServersWhoAgreeSoFarOnP == Main.QUORUM) // There's a quorum on what the history should be
				{
					quorumAgreed = true;
					int leaderID = PServer.Prepare2Leader(p.prepareNumber());					
					acceptMsgs.offer(new Accept(leaderID, slotNumber, p, false));
					break;
				}				
			}	
			if (!quorumAgreed) // There's no quorum on what the history should be
			{
				acceptMsgs.offer(new Accept(parent.serverID, slotNumber, Proposal.noop(parent.prepareNumber), false));
			}
		}
	}
	
	private void replayHistory() throws IOException, InterruptedException
	{
		parent.println("top of replay, acceptMsgs size is " + acceptMsgs.size());
		assert acceptMsgs.size() == parent.history.size(); // should always pass
		parent.println("passed assertion");
		for (Accept a : acceptMsgs)
		{
			parent.println("top of halt");
			if (!halt)
			{
				parent.println("top of halt");
				parent.sRouter.broadcast(a);				
				Thread.sleep(200);
			}
			else break; // breaks and then exits in run
		}
		if (acceptMsgs.size() == 0)
		{
			parent.setState(ServerStates.COLLECTING_CLIENT_OUT);	
		}
	}
}
