package routers;

import java.io.IOException;

import paxos.CleanupAndReplayThread;
import paxos.HeartbeatThread;
import paxos.Main;
import paxos.PServer;
import paxos.ServerStates;
import commands.Accept;
import commands.Ack;
import commands.ClientIn;
import commands.ClientOut;
import commands.Command;
import commands.Cmd;
import commands.Prepare;
//import commands.History;
import commands.Proposal;


public class SRouter extends Channel
{
	protected boolean verbose;
	protected PServer parent;
	
	public SRouter(PServer parent, ChannelSettings channel) throws IOException 
	{
		super(channel);		
		this.parent = parent;
		verbose = parent.verbose;
	}

	@Override
	public void switchBoard(Command cmd) throws IOException 
	{
		stateSwitchBoard(cmd);		
	}

	public void stateSwitchBoard(Command cmd) throws IOException
	{		
		println(String.format("SROUTER: Routing \"%s\" to: %s", cmd, parent.getState()));
		switch(parent.getState())
		{
		
		case INITIAL:
		{
			initialSwitchBoard(cmd);
			break;
		}
		case COLLECTING_HISTORY:
		{
			collectingHistorySwitchBoard(cmd);
			break;
		}
		case COLLECTING_ACKS:
		{
			collectingAcksSwitchBoard(cmd);
			break;
		}
		case COLLECTING_CLIENT_OUT:
		{
			collectingClientOutsSwitchBoard(cmd);
			break;
		}
		default:
		{			
			break;
		}

		}
	}

	public void initialSwitchBoard(Command cmd) throws IOException
	{
		switch (cmd.command())
		{
		case Cmd.PREPARE:
		{
			Prepare prep = (Prepare)cmd;
			parent.prepareNumber = prep.prepareNumber();
			if (parent.isLeader())
			{
				println(String.format("server %d is leader", prep.prepareNumber() % Main.NUM_SERVERS));
				HeartbeatThread heartbeat = new HeartbeatThread(ChannelSettings.defaultChannel);
				heartbeat.start();
				parent.setState(ServerStates.COLLECTING_HISTORY);
			}
			broadcast(parent.history);
			break;
		}
		case Cmd.ACCEPT:
		{
			if (!parent.isLeader()) // This check is probably not necessary 
			{
				Accept a = (Accept)cmd;
				Ack ack = new Ack(a, parent.serverID);
				parent.setState(ServerStates.COLLECTING_ACKS);
				broadcast(ack);
			}
			break;
		}
		default:
		{
			break;
		}

		}
	}

	public void collectingHistorySwitchBoard(Command cmd) throws IOException // only leader should ever be in this state
	{		
		switch (cmd.command())
		{

		case Cmd.HISTORY:
		{
			parent.add(cmd);
			if (parent.quorum())
			{
				println("HISTORY SWITCH: reached quorum");
//				parent.setState(ServerStates.COLLECTING_ACKS);		// need to broadcast own ack before collecting acks		
				parent.thread = CleanupAndReplayThread.CleanAndReplay(parent);
				parent.thread.start();
			}
			break;
		}
		case Cmd.ACCEPT:
		{
			parent.setState(ServerStates.COLLECTING_ACKS);
			Accept a = (Accept)cmd;
			Ack ack = new Ack(a, parent.serverID);
			broadcast(ack);
			break;
		}
		default:
		{
			break;
		}

		}
	}

	public void collectingAcksSwitchBoard(Command cmd) throws IOException
	{
		println("top of ackSwitch: " + cmd);
		switch (cmd.command())
		{

		case Cmd.ACK:
		{
			parent.add(cmd);
			println(String.format("Adding %s to ackCount: ", cmd));
			if (parent.quorum())
			{
				println("reached quorum of acks");
				parent.resetAcks();
				Ack ack = (Ack)cmd;
				Accept a = ack.accept();
				Proposal p = a.proposal();
				parent.history.add(a.slotNumber(), p);
				if (parent.isLeader())
					parent.setState(ServerStates.COLLECTING_CLIENT_OUT);
				else parent.setState(ServerStates.INITIAL);
				if (a.share()) broadcast(new ClientIn(p.value()));						
			}
			break;
		}
		default:
		{
			println("default ack");
			break;
		}

		}
	}

	public void collectingClientOutsSwitchBoard(Command cmd) throws IOException
	{
		switch (cmd.command())
		{

		case Cmd.CLIENT_OUT:
		{
			ClientOut cOut = (ClientOut)cmd;
			Proposal p = new Proposal(parent.prepareNumber, cOut.message());
			Accept a = new Accept(parent.serverID, parent.history.size(), p, true);
//			parent.setState(ServerStates.COLLECTING_ACKS);
			broadcast(a);
			break;
		}
		
		case Cmd.ACCEPT:
		{
			if (parent.isLeader())
			{				
				Accept a = (Accept)cmd;
				Ack ack = new Ack(a,  parent.serverID);
				parent.setState(ServerStates.COLLECTING_ACKS);
				broadcast(ack);
			}
			break;
		}
		
		default:
		{
			break;
		}

		}
	}
	
	@Override
	public void broadcast(Command cmd) throws IOException
	{
		parent.println("broadcasting " + cmd);
		super.broadcast(cmd);
		parent.println("sent " + cmd);
	}

	public void println(String s)
	{
		if (verbose)			
			System.out.println(s);
	}
}
